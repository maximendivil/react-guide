import React, { PureComponent } from "react";
import Person from "./Person/Person";

class Persons extends PureComponent {
    constructor(props) {
        super(props);
        console.log('[Persons.js] Inside constructor()');
        this.lastPersonRef = React.createRef();
    }

    // static getDerivedStateFromProps(props, state) {
    //     console.log('[Persons.js] getDerivedStateFromProps');
    // }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('[UPDATE Persons.js] getSnapshotBeforeUpdate');
        return {message: 'snapshot'};
    }

    componentDidMount() {
        console.log('[Persons.js] Inside componentDidMount()');
        this.lastPersonRef.current.focus();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('[UPDATE Persons.js] Inside componentDidUpdate()');
        console.log(snapshot);
    }

    render() {
        console.log('[Persons.js] Inside render()');

        return this.props.persons.map((person, index) => {
            return <Person
                click={() => this.props.clicked(index)}
                name={person.name}
                age={person.age}
                key={person.id}
                position={index}
                ref={this.lastPersonRef}
                changed={(event) => this.props.changed(event, person.id)} />
        });
    }
}

export default Persons;

import React, { useEffect, useRef, useContext } from 'react';
import classes from './Cockpit.css';
import AuthContext from '../../context/auth-context';

const cockpit = (props) => {
  const toogleBtnRef = useRef(null);
  const authContext = useContext(AuthContext);

  useEffect(() => {
    console.log('[Cockpit.js] useEffect');
    toogleBtnRef.current.click();

    return () => {
      console.log('[Cockpit.js] cleanup work...');
    }
  }, []);

  const assignedClasses = [];
  let btnClass = classes.Button;

  if (props.showPersons) {
    btnClass = [classes.Button, classes.Red].join(' ');
  }

  if (props.personsLength <= 2) {
    assignedClasses.push(classes.red)
  }
  if (props.personsLength <= 1) {
    assignedClasses.push(classes.bold)
  }

  return (
    <>
      <h1>{props.appTitle}</h1>
      <p className={assignedClasses.join(' ')}>I'm working!</p>
      <button
        ref={toogleBtnRef}
        className={btnClass}
        onClick={props.clicked} >Show / Hide</button>
      <button onClick={authContext.login}>Log in</button>
    </>
  );
}

export default React.memo(cockpit);

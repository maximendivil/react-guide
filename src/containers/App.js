import React, { Component } from 'react';
import classes from './App.css';
import Cockpit from '../components/Cockpit/Cockpit';
import Persons from '../components/Persons/Persons';
import withClass from '../hoc/withClass';
import AuthContext from '../context/auth-context';

class App extends Component {
  constructor(props) {
    super(props);
    console.log('[App.js] Inside constructor()');
  }

  state = {
    persons: [
      { id: 1, name: 'Max', age: 26 },
      { id: 2, name: 'Manu', age: 29 },
      { id: 3, name: 'Stephanie', age: 24 }
    ],
    showPersons: false,
    showCockpit: true,
    toggleCounter: 0,
    authenticated: false
  };

  static getDerivedStateFromProps(props, state) {
    console.log('[App.js] Inside getDerivedStateFromProps()', props);
    return state;
  }

  componentDidMount() {
    console.log('[App.js] Inside componentDidMount()');
  }

  componentDidUpdate() {
    console.log('[App.js] componentDidUdate()');
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[App.js] shouldComponentUpdate()');
    return true;
  }

  togglePersons = () => {
    const doesShow = this.state.showPersons;
    this.setState((prevState, props) => {
      return {
        showPersons: !doesShow,
        toggleCounter: prevState.toggleCounter + 1
      }
    });
  }

  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({
      persons: persons
    });
  }

  changeNameHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(person => person.id === id);

    const person = {
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({
      persons: persons
    });
  }

  loginHandler = () => {
    this.setState({ authenticated: true });
  }

  render() {
    console.log('[App.js] Inside render()');
    let persons = null;

    if (this.state.showPersons) {
      persons =
        <Persons
          persons={this.state.persons}
          clicked={this.deletePersonHandler}
          changed={this.changeNameHandler} />
    }

    return (
      <>
        <button onClick={() => { this.setState({ showPersons: true }) }}>Show persons</button>
        <button onClick={() => { this.setState({ showCockpit: false }) }}>Remove cockpit</button>
        <AuthContext.Provider value={{ authenticated: this.state.authenticated, login: this.loginHandler }}>
          {
            this.state.showCockpit ?
              <Cockpit
                appTitle={this.props.title}
                showPersons={this.state.showPersons}
                personsLength={this.state.persons}
                clicked={this.togglePersons} />
              : null
          }
          {persons}
        </AuthContext.Provider>
      </>
    );
  }
}

export default withClass(App, classes.App);
